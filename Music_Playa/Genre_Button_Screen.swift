//
//  Genre_Button_Screen.swift
//  Music_Playa
//
//  Created by Charlie  Velazquez on 11/27/17.
//  Copyright © 2017 GoonPlatoon. All rights reserved.
//  pay tanner for his contributions
//

import UIKit
import MediaPlayer

class Genre_Button_Screen: UIViewController {
    
    var musicPlayer = MPMusicPlayerController.applicationMusicPlayer

    @IBOutlet weak var Genre_field: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func genreButtonTapped(_ sender: UIButton) {
        MPMediaLibrary.requestAuthorization{ (status) in
            if status == .authorized {
                self.playGenre(genre: self.Genre_field.text!)
            }
        }
    }
    @IBAction func stopButtonTapped(_ sender: UIButton) {
        musicPlayer.stop()
    }
    @IBAction func nextButtonTapped(_ sender: UIButton) {
        musicPlayer.skipToNextItem()
    }
    
    func playGenre(genre:String){
        musicPlayer.stop()
        let query = MPMediaQuery()
        let predicate = MPMediaPropertyPredicate(value: genre, forProperty : MPMediaItemPropertyGenre)
        query.addFilterPredicate(predicate)
        musicPlayer.setQueue(with: query)
        musicPlayer.shuffleMode = .songs
        musicPlayer.play()
        
    }
}
